<?php
/**
 * @file
 *
 * Views cache plugin file. Provides prefetch caching.
 *
 */

/**
 * Caching plugin that provides static caching for sake of conistency.
 */
class cache_graceful_views_plugin_cache_graceful extends views_plugin_cache {
  static private $atomic = array();
  
  function cache_start() { 
    /* do nothing */
  }

  function options_form(&$form, &$form_state) {
    $options = array(60, 300, 1800, 3600, 21600, 518400);
    $options = drupal_map_assoc($options, 'format_interval');
    $options = array(-1 => t('Never cache')) + $options;

    $form['results_expire'] = array(
      '#type' => 'textfield',
      '#title' => t('Query results expiration'),
      '#description' => t(
        'The length of time in seconds raw query results should be cached. (blank = ' . 
        variable_get('cache_graceful_expire', CACHE_GRACEFUL_EXPIRE) .
        ', 0 = disabled)'
      ),
      '#default_value' => $this->options['results_expire'],
    );
    $form['results_prefetch'] = array(
      '#type' => 'textfield',
      '#title' => t('Query results prefetch'),
      '#description' => t('The length of time in seconds before prefetch should be performed. (blank = ' .
        variable_get('cache_graceful_prefetch', CACHE_GRACEFUL_PREFETCH) .
        ', 0 = disabled)'
      ),
      '#default_value' => $this->options['results_prefetch'],
    );
  }
  
  function summary_title() {
    return t('Graceful');
  }

  function cache_get($type) {
    if ($type != 'results') return FALSE;
    
    $key = $this->get_results_key();
    
    $args = array('_cache_graceful_views_execute', serialize($this->view));
    // Send globals to background process
    $args[] = $GLOBALS['pager_page_array'];
    $args[] = $GLOBALS['pager_total'];
    $args[] = $GLOBALS['pager_total_items'];
    
    // Load graceful cache
    $data = cache_graceful($key, $args, 'cache', $this->options['results_expire'], $this->options['results_prefetch']);
    
    // Put results back in local scope
    $this->view->result = $data['result'];
    $this->view->total_rows = $data['total_rows'];
    $this->view->execute_time = 0;
    
    // Retrieve globals
    if (!isset($GLOBALS['pager_page_array'])) {
      $GLOBALS['pager_page_array'] = array();
    }
    if (!isset($GLOBALS['pager_total'])) {
      $GLOBALS['pager_total'] = array();
    }
    if (!isset($GLOBALS['pager_total_items'])) {
      $GLOBALS['pager_total_items'] = array();
    }
    $GLOBALS['pager_page_array']  = array_merge($GLOBALS['pager_page_array'],  $data['pager_page_array']);
    $GLOBALS['pager_total']       = array_merge($GLOBALS['pager_total'],       $data['pager_total']);
    $GLOBALS['pager_total_items'] = array_merge($GLOBALS['pager_total_items'], $data['pager_total_items']);

    return TRUE;
  }

  function cache_set($type) { 
    if ($type != 'results') return FALSE;
    return TRUE;
  }
}
